*** Settings ***
Library    RequestsLibrary

*** Variables ***
${Base_URL}    https://9kd6y1tfw2.execute-api.ap-southeast-1.amazonaws.com/test

*** Test Cases ***
TC_Get_Ping
    Create Session    my_session    ${Base_URL}
    ${response}=    get request    my_session    /ping
    Status Should Be    200
    Should Be Equal    ping-pong    ${response.text}
