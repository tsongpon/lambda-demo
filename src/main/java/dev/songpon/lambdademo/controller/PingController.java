package dev.songpon.lambdademo.controller;

import dev.songpon.lambdademo.model.Person;
import dev.songpon.lambdademo.service.PingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
public class PingController {

    @Autowired
    private PingService pingService;

    @GetMapping("/ping")
    public String ping() {
        return pingService.ping();
    }

    @GetMapping("/person")
    public Mono<Person> getPerson() {
        return Mono.just(new Person("Songpon tum", "Imyen"));
    }
}
