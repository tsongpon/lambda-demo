package dev.songpon.lambdademo.service;

import org.springframework.stereotype.Service;

@Service
public class PingService {
    public String ping() {
        return "ping-pong";
    }
}
