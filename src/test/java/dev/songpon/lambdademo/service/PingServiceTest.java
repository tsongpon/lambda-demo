package dev.songpon.lambdademo.service;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PingServiceTest {

    @Test
    void ping() {
        assertEquals("ping-pong", new PingService().ping());
    }
}